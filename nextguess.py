from sklearn.linear_model import LinearRegression
import numpy as np
import pandas as pd

limra = input()
a = [int(x) for x in limra.split(",")]
fin = np.array(a)
y = []
for i in range(1,len(fin)+1):
    y.append(i)
sin = np.array(y)
model = LinearRegression(fit_intercept = True)
model.fit(sin[:, np.newaxis],fin)

cool = len(fin) + 1
x_test = np.array(cool)
prediction = model.predict(x_test)
print(prediction)

# model.coef_[0]
# model.intercept_



