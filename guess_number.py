from sklearn.linear_model import LinearRegression

x_train = [[1],[2],[3],[4],[5]]
y_train = [[2],[4],[6],[8],[10]]

lm = LinearRegression()

lm.fit(x_train,y_train)

x_test = [[6]]

prediction = lm.predict(x_test)

print(prediction)